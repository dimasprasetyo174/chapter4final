import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { searchMovie, getMovieList } from "../api";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

function Pencarian() {
  const navigate = useNavigate();
  const [popularMovie, setPopularMovie] = useState([]);

  useEffect(() => {
    getMovieList().then((result) => {
      setPopularMovie(result);
    });
  }, []);

  const search = async (q) => {
    if (q.length > 3) {
      const query = await searchMovie(q);
      setPopularMovie(query.results);
    }
  };

  const PopularMovieList = () => {
    return popularMovie.map((movie, i) => {
      return (
        <div className="movie-wrapper " key={i}>
          <div className="movie-title">{movie.title}</div>
          <img className="movie-image" alt=" " src={`${process.env.REACT_APP_BASEIMGURL}/${movie.poster_path}`} />
          <div className="movie-rate">{movie.vote_average}</div>
        </div>
      );
    });
  };
  return (
    <div>
      <div>
        <Navbar>
          <Container>
            <Navbar.Brand onClick={() => navigate("/")}>
              <strong>Movielist</strong>
            </Navbar.Brand>
            <input placeholder="  what do you want to wacth ?" className="movie-search" onChange={({ target }) => search(target.value)} />
            <Nav>
              <Button className="me-3" variant="outline-danger">
                Login
              </Button>
              <Button variant="danger">Register</Button>
            </Nav>
          </Container>
        </Navbar>
      </div>
      <div className="Popular mt-4 ms-2">
        <div className="popular-top">
          <h2>Movie</h2>
        </div>
        <div className="movie-container">
          <PopularMovieList />
        </div>
      </div>
    </div>
  );
}

export default Pencarian;
