import React from "react";
import { Button, Col, Container, Row } from "react-bootstrap";
import NavbarItem from "../components/NavbarItem";

function LandingPage() {
  return (
    <div className="Home">
      <NavbarItem />
      <div className="content">
        <Container fluid className="text-white d-flex justify-content-start mt-5 pt-5">
          <Row>
            <Col className="col-8">
              <div>
                <h1 className="mt-5">Doctor Strange In The Multiverse Of Madness</h1>
              </div>
              <div>
                <p className=" mt-2">lorem ipsum dolot sit amet, consectur adipiscing eli, sed do eiusmood tempar incididunt ut labore et dolore magna aliqua</p>
              </div>
              <Button variant="danger">
                <strong>WACTH TRAILER</strong>
              </Button>
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
}

export default LandingPage;
