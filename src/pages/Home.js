import React from "react";
import LandingPage from "./LandingPage";
import Popular from "./Popular";

function Home() {
  return (
    <div>
      <LandingPage />
      <Popular />
    </div>
  );
}

export default Home;
