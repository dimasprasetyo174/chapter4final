import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import Detail from "./pages/Detail";
import Home from "./pages/Home";
import Pencarian from "./pages/Pencarian";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route>
          <Route path="/:title" component={Detail} />
          <Route path="/pencarian" element={<Pencarian />} />
          <Route path="/" element={<Home />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
